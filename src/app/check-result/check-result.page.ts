import { Component} from '@angular/core';
import {LoginRequestMessage, VerifytoLogin,ApiNode, CredentialStored, VerifyRequestMessage, MessageType, CredentialRequired} from 'siriusid-sdk';
import {Listener, TransferTransaction, EncryptedMessage, PublicAccount,NetworkType} from 'tsjs-xpx-chain-sdk';
import {ChangeDataService} from '../../services/change-data.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import {ClientInfoService} from 'src/services/client-info.service';
import io from 'socket.io-client';
import { LoginVerificationService } from '../services/login-verification/login-verification.service';

@Component({
  selector: 'app-check-result',
  templateUrl: './check-result.page.html',
  styleUrls: ['./check-result.page.scss'],
})
export class CheckResultPage{

  linkLogin;
  linkCredential;

  img = null;
  showImg = false;
  URL;
  currentNode = "api-1.testnet2.xpxsirius.io";
  status = null;
  lived = false;
  subscribe;
  listener: Listener;
  connect = false;
  credential_id = "visa";
  credential;
  alert = true;
  warning = true;

  constructor(
    private changeDataService: ChangeDataService,
    private router: Router,
    public alertController: AlertController,
    private clientInfo: ClientInfoService,
    private loginVerificationService: LoginVerificationService
    ) {
    
  }

  ionViewWillEnter(){
    ApiNode.apiNode = "https://"+this.currentNode;
    ApiNode.networkType = NetworkType.TEST_NET;
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    if (this.clientInfo.lightVerification){
      this.lightVerification();
    }
    else this.loginRequestAndVerify();
  }

  async lightVerification(){
    this.showImg = true;
    console.log("Verify Flow");
    const url = "https://b51ae20b.ngrok.io";
    
    this.credential = CredentialRequired.create(this.credential_id);
    const loginRequestMessage = VerifyRequestMessage.create(this.changeDataService.publicKeydApp,[this.credential],MessageType.VERIFY_LIGHT,url);
    const sessionToken = loginRequestMessage.getSessionToken();
    this.img = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    console.log(loginRequestMessage);

    const socket = io(url);
    socket.on(sessionToken,async data => {
      console.log(data);
      this.clientInfo.clientInfo = [await this.getCredential(data)];
      console.log(data.credentials);
      console.log(this.clientInfo.clientInfo);
      socket.close();
      this.router.navigate(['/show-result']);
    })

    socket.on("connect_failed", data => {
      console.log("connect failed");
    })

    socket.on("connect_error", data => {
      console.log("connect_error");;
    })

    socket.on("disconnect", data => {
      console.log("disconnect");
    })
  }
  
  async getCredential(data){
    const credentialStored = new CredentialStored(data.credentials[0]['keyDecrypt'],data.credentials[0]['credentialHash']);
    return await credentialStored.getCredential(data.credentials[0]['credentialHash'],data.credentials[0]['keyDecrypt'],data.publicKey);
  }

  // for QR code
  async loginRequestAndVerify(){
    this.connect = false;
    this.credential = CredentialRequired.create(this.credential_id);
    const loginRequestMessage = VerifyRequestMessage.create(this.changeDataService.publicKeydApp,[this.credential],MessageType.VERIFY_HARD);
    const sessionToken = loginRequestMessage.getSessionToken();
    //console.log('sessionToken = ' + sessionToken);
    this.img = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect){
        this.warning = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection(sessionToken);
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg = true;
      }
      else if(this.connect){
        clearInterval(interval);
      }
    }, 1000);
  }
  
  async connection(sessionToken) {
    
    this.listener.open().then(() => {
      this.warning = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {

        let verify = this.loginVerificationService.verify(transaction, sessionToken, [this.credential], this.changeDataService.privateKeydApp);

        if (verify){
          this.connect = true;
          console.log("Transaction matched");          
          let message = VerifytoLogin.getMessage();
          // this.clientInfo.name = message.payload.credentials[0].content[0][1];
          this.changeDataService.addressSiriusid = transaction.signer.address.plain();
          this.clientInfo.clientInfo = this.loginVerificationService.credentials;
          // this.storeClientInfo(transaction);
          this.subscribe.unsubscribe();
          this.router.navigate(['/show-result']);
        }
        else console.log("Transaction not match");  
               
      });
    })
    
  }

  refresh(){
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }

  // storeClientInfo(transaction:any){
  //   const transferTx = transaction as TransferTransaction;
  //   const transferTxMessage = EncryptedMessage.createFromPayload(transferTx.message.payload);
  //   const plainMessage = EncryptedMessage.decrypt(transferTxMessage,this.changeDataService.privateKeydApp,<PublicAccount>transferTx.signer);
    
  //   const JSONtransferTxmessage = JSON.parse(plainMessage.payload);
  //   this.clientInfo.clientInfo = JSONtransferTxmessage.payload.credentials;
  //   console.log(JSONtransferTxmessage.payload.credentials);
  // }

  // checkResult(){
  //   this.connect = true;
  //   this.subscribe.unsubscribe();
  //   this.router.navigate(['/check-result']);
  // }

  applyForm(){
    this.connect = true;
    if (!this.clientInfo.lightVerification){
      this.subscribe.unsubscribe();
    }
    this.router.navigate(['/home']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }
}
