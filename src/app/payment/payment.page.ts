import { Component, OnInit } from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';
import { ChangeDataService } from 'src/services/change-data.service';
import { TransferTransaction, TransactionMapping, Deadline, Address, EncryptedMessage, PlainMessage, NetworkType, UInt64, Listener, TransactionType } from 'tsjs-xpx-chain-sdk';
import { TransactionRequestMessage } from 'siriusid-sdk';
@Component({
  selector: 'app-payment',
  templateUrl: './payment.page.html',
  styleUrls: ['./payment.page.scss'],
})
export class PaymentPage implements OnInit {

  linkPayment;

  subscribe;
  listener: Listener;
  currentNode = "api-1.testnet2.xpxsirius.io";
  connect = false;

  fname;
  lname;
  appID = "MA01234";
  amount = "10 USD";
  purpose = "Payment for eVisa Application Fee";
  address;
  token;
  img;
  showImg =false;
  warning = false;
  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
    private changeDataService: ChangeDataService
  ) {
    let content = this.clientInfo.clientInfo[0].content;
    this.fname = content[0][1];
    this.lname = content[1][1];
    this.address = this.changeDataService.addressdApp.pretty();
    
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    this.loginRequestAndVerify();
  }

  async createTransaction(){
    let tx = TransferTransaction.create(
      Deadline.create(),
      this.changeDataService.addressdApp,
      [],
      PlainMessage.create(this.purpose),
      NetworkType.TEST_NET,
      new UInt64([0,0])
    );
    const msg = new TransactionRequestMessage(this.purpose,tx,"");
    //console.log(msg);

    this.img = await msg.generateQR();
    this.linkPayment = await msg.universalLink();
  }

  ngOnInit() {
  }

  /**
   * Generate random string of sesion token
   */
  private  generateToken() {
      
    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 64; i++) {

        outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

    }
    return outString;
  }

  async loginRequestAndVerify(){
    this.connect = false;
    // this.token = this.generateToken();
    const img = await this.createTransaction();
    //console.log('sessionToken = ' + sessionToken);

    this.connection();
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect){
        this.warning = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection();
      }
      else if(listenerAsAny.webSocket.readyState == 1 && !this.connect){
        this.showImg =true;
      }
      else if(this.connect){
        clearInterval(interval);
      }
    }, 1000);
  }
  
  async connection() {
    this.warning = false;
    this.listener.open().then(() => {
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        console.log(transaction);
        let tx = transaction as TransferTransaction;
        if (tx.type == TransactionType.TRANSFER){
          if (tx.message.payload == this.purpose && transaction.signer.address.plain() == this.changeDataService.addressSiriusid){
            this.connect = true;
            console.log("Transaction matched");          
            this.subscribe.unsubscribe();
            this.router.navigate(['/welcome']);
          }
          else{
            console.log("Transaction not match");
          }
        }     
      });
    })
    
  }

  refresh(){
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.connect = true;
    this.subscribe.unsubscribe();
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.connect = true;
    this.subscribe.unsubscribe();
    this.router.navigate(['/home']);
  }

  deeplink(){
    window.location = this.linkPayment;
  }
}
