import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  // { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  { path: 'home', loadChildren: './home/home.module#HomePageModule'},
  { path: 'submit-infomation', loadChildren: './submit-infomation/submit-infomation.module#SubmitInfomationPageModule' },
  // { path: 'payment', loadChildren: './payment/payment.module#PaymentPageModule' },
  { path: 'welcome', loadChildren: './welcome/welcome.module#WelcomePageModule' },
  { path: 'payment', loadChildren: './payment/payment.module#PaymentPageModule' },
  { path: 'check-result', loadChildren: './check-result/check-result.module#CheckResultPageModule' },
  { path: 'show-result', loadChildren: './show-result/show-result.module#ShowResultPageModule' },
  { path: '**', redirectTo: 'home', pathMatch: 'full' },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
