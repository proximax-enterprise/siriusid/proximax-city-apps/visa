import { Component} from '@angular/core';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-show-result',
  templateUrl: './show-result.page.html',
  styleUrls: ['./show-result.page.scss'],
})
export class ShowResultPage{

  img = null;
  URL;
  status = null;
  lived = false;
  subscribe;
  flag = true;
  continue = true;
  connect = false;
  box1 = false;
  box2 = false;
  entryDate;
  exitDate;

  /** Passport Information*/
  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  passportNumber;
  expiryDate;
  passportType;

  /** Visa Information*/
  validFrom ;
  validTo;
  country;
  allowEntry;
  allowExit;
  purpose;
  tempResidential;
  contact;
  aggree;

  constructor(
    private router: Router,
    public clientInfo: ClientInfoService,
    ) {
      let content = this.clientInfo.clientInfo[0].content;
      this.fname = content[0][1];
      this.lname = content[1][1];
      this.birth = content[2][1];
      this.address = content[3][1];
      this.phoneNumber = content[4][1];
      this.email = content[5][1];
      this.passportNumber = content[6][1];
      this.passportType = content[7][1];
      this.expiryDate = content[8][1];
      this.validFrom = content[9][1];
      this.validTo = content[10][1];
      this.country = content[11][1];
      this.allowEntry = content[12][1];
      this.allowExit = content[13][1];
      this.purpose = content[14][1];
      this.tempResidential = content[15][1];
      this.contact = content[16][1];
  }
  

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }

}
