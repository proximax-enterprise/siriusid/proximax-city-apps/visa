import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class StorageBucketService {

  apiEndpoint = 'https://sid-s3-storage-api.proximaxtest.com/file'
  username = environment.S3Credentials.username;
  password = environment.S3Credentials.password;

  headers = new Headers();

  constructor() {
    this.headers.set('Authorization', 'Basic ' + btoa(this.username + ":" + this.password));
  }

  async postFile(dataUrl: string, fileKey: string, extension: string): Promise<any> {

    let file = this.dataURLtoFile(dataUrl, `${fileKey}.${extension}`);

    let formData = new FormData();
    formData.append('file', file);
    formData.append('id', fileKey);
    console.log(formData);

    let response = await fetch(this.apiEndpoint, {
      method: 'POST',
      headers: this.headers,
      body: formData
    })

    console.log(response);

    return response;
  }

  async getFile(fileKey: string): Promise<any> {

    const getEndpoint = `${this.apiEndpoint}/${fileKey}`;

    let response = await fetch(getEndpoint, {
      method: 'GET',
      headers: this.headers,
    })

    let data = await response.json();
    let url = data.url;

    response = await fetch(url);
    data = await response.json();

    return data;
  }

  async getImageUrl(fileKey: string): Promise<any> {
    const getEndpoint = `${this.apiEndpoint}/${fileKey}`;

    let response = await fetch(getEndpoint, {
      method: 'GET',
      headers: this.headers,
    })

    let data = await response.json();

    return data.url;
  }


  // https://gist.github.com/ibreathebsb/a104a9297d5df4c8ae944a4ed149bcf1
  dataURLtoFile = (dataurl: string, filename: string) => {

    const arr = dataurl.split(',');
    const mime = arr[0].match(/:(.*?);/)[1];
    const bstr = atob(arr[1]);
    let n = bstr.length;
    const u8arr = new Uint8Array(n);
    while (n) {
      u8arr[n - 1] = bstr.charCodeAt(n - 1)
      n -= 1 // to make eslint happy
    }
    console.log('the mime is ' + mime);
    return new File([u8arr], filename, { type: mime })
  }

  // experiments

  strToFile = (data, fileName) => {
    var blob = new Blob([data]);
    return new File([blob], fileName);
  }

  postJSON = async (data, fileKey, extension) => {

    let file = this.strToFile(data, `${fileKey}.${extension}`);

    console.log(file);
    let formData = new FormData();
    formData.append('file', file);
    formData.append('id', fileKey);

    let response = await fetch(this.apiEndpoint, {
      method: 'POST',
      headers: this.headers,
      body: formData
    })

    console.log(response);

    return response;
  }

  temp = async () => {
    let jsonData = JSON.stringify({ name: 'lim' });

    let data = await this.postJSON(jsonData, 'test', 'json');
    console.log(data);
  }

}
