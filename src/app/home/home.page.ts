import { Component } from '@angular/core';
import { LoginRequestMessage, VerifytoLogin, ApiNode, CredentialRequired } from 'siriusid-sdk';
import { Listener, TransferTransaction, EncryptedMessage, PublicAccount, NetworkType } from 'tsjs-xpx-chain-sdk';
import { ChangeDataService } from '../../services/change-data.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { ClientInfoService } from 'src/services/client-info.service';
import { LoginVerificationService } from '../services/login-verification/login-verification.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  linkLogin;
  linkCredential;

  img = null;
  showImg = false;
  URL;
  currentNode = "api-1.testnet2.xpxsirius.io";//"bctestnet2.brimstone.xpxsirius.io";
  status = null;
  lived = false;
  subscribe;
  listener: Listener;
  connect = false;
  credential_id = "passport";
  credential;
  alert = true;
  warning = false;
  flag = true;
  step1 = false;

  constructor(
    private changeDataService: ChangeDataService,
    private router: Router,
    public alertController: AlertController,
    private clientInfo: ClientInfoService,
    private loginVerificationService: LoginVerificationService
  ) {
    console.log('api node: ' + this.currentNode);
  }

  ionViewWillEnter() {
    ApiNode.apiNode = "https://" + this.currentNode;
    ApiNode.networkType = NetworkType.TEST_NET;
    this.changeDataService.apiNode = this.currentNode;
    this.changeDataService.updateWebsocket();
    this.listener = new Listener(this.changeDataService.ws, WebSocket);
    if (this.flag) {
      this.presentAlert2();
      this.flag = false;
    } else {
      this.step1 = false;
      this.presentAlert();
    }
  }
  // for QR code
  async loginRequestAndVerify() {
    this.connect = false;
    this.step1 = true;
    let arr = ['First Name', 'Last Name', 'Date of Birth', 'Address', 'Phone number', 'Email', 'Passport ID', 'Type of passport', 'Expiry date'];
    this.credential = CredentialRequired.create(this.credential_id, arr);
    const loginRequestMessage = LoginRequestMessage.create(this.changeDataService.publicKeydApp, [this.credential]);
    const sessionToken = loginRequestMessage.getSessionToken();
    //console.log('sessionToken = ' + sessionToken);
    this.img = await loginRequestMessage.generateQR();
    this.linkLogin = await loginRequestMessage.universalLink();

    this.connection(sessionToken);
    let interval = setInterval(async () => {
      const listenerAsAny = this.listener as any;
      console.log(listenerAsAny.webSocket.readyState);
      if (listenerAsAny.webSocket.readyState !== 1 && listenerAsAny.webSocket.readyState !== 0 && !this.connect) {
        this.warning = true;
        this.listener = new Listener(this.changeDataService.ws, WebSocket);
        this.connection(sessionToken);
      }
      else if (listenerAsAny.webSocket.readyState == 1 && !this.connect) {
        this.showImg = true;
      }
      else if (this.connect) {
        clearInterval(interval);
      }
    }, 1000);
  }

  async connection(sessionToken) {

    this.listener.open().then(() => {
      this.warning = false;
      this.subscribe = this.listener.confirmed(this.changeDataService.addressdApp).subscribe(transaction => {
        console.log(this.credential);
        console.log(transaction);

        let verify = this.loginVerificationService.verify(transaction, sessionToken, [this.credential], this.changeDataService.privateKeydApp);

        if (verify) {
          this.connect = true;
          console.log("Transaction matched");
          let message = this.loginVerificationService.message;
          // this.clientInfo.name = message.payload.credentials[0].content[0][1];
          this.changeDataService.addressSiriusid = transaction.signer.address.plain();
          // this.storeClientInfo(transaction);
          this.clientInfo.clientInfo = this.loginVerificationService.credentials;
          this.subscribe.unsubscribe();
          this.router.navigate(['/submit-infomation']);
        }
        else console.log("Transaction not match");

      });
    })

  }

  refresh() {
    this.subscribe.unsubscribe();
    this.loginRequestAndVerify();
  }

  private static generateRandom() {

    let outString: string = '';
    let inOptions: string = 'abcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 64; i++) {

      outString += inOptions.charAt(Math.floor(Math.random() * inOptions.length));

    }
    return outString;
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Asia Visa will ask you to share',
      message: '<div class="alert"><img src="assets/icon-credentials-digital-passport.svg"><p>ProximaXCity Passport</p></div>',
      buttons: [
        {
          text: 'Login',
          cssClass: 'primary',
          handler: () => {
            this.loginRequestAndVerify();
          }
        }
      ],
      cssClass: "boxAlert"
    });

    await alert.present();
  }

  async presentAlert2() {
    const alert = await this.alertController.create({
      header: 'Welcome to eVisa Application',
      // message: '<div class="alert"><img src="assets/icon-credentials-employment-pass.svg"><p>ProximaXCity ID</p></div>',
      buttons: [
        {
          text: 'Create Visa',
          cssClass: 'primary',
          handler: () => {
            this.presentAlert();
          }

        },
        {
          text: 'Hard Verification',
          cssClass: 'primary',
          handler: () => {
            this.checkResult('hard');
          }
        },

        {
          text: 'Light Verification',
          cssClass: 'primary',
          handler: () => {
            this.checkResult('light');
          }
        },
      ],

      cssClass: "boxAlert"
    });

    await alert.present();
  }

  checkResult(param: string) {
    if (param == "hard") {
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light") {
      this.clientInfo.lightVerification = true;
    }

    this.connect = true;
    // this.subscribe.unsubscribe();
    this.router.navigate(['/check-result']);
  }

  applyForm() {
    this.connect = true;
    this.subscribe.unsubscribe();
    this.router.navigate(['/home']);
  }

  deeplink(param: string) {
    if (param == "login") {
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }

}
