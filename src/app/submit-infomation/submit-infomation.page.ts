import { Component} from '@angular/core';
import {CredentialRequestMessage, Credentials} from 'siriusid-sdk';
import {ChangeDataService} from '../../services/change-data.service';
import {ClientInfoService} from '../../services/client-info.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
@Component({
  selector: 'app-submit-infomation',
  templateUrl: './submit-infomation.page.html',
  styleUrls: ['./submit-infomation.page.scss'],
})
export class SubmitInfomationPage{

  linkLogin;
  linkCredential;

  img = null;
  URL;
  status = null;
  lived = false;
  subscribe;
  flag = true;
  continue = true;
  connect = false;
  box1 = false;
  box2 = false;
  entryDate;
  exitDate;

  /** Passport Information*/
  fname;
  lname;
  birth;
  address;
  phoneNumber;
  email;
  passportNumber;
  expiryDate;
  passportType;

  /** Visa Information*/
  validFrom = "15/02/2020";
  validTo = "15/02/2021";
  allowEntry;
  allowExit;
  purpose;
  tempResidential;
  aggree;
  emergencyContact;
  country = "Malaysia";

  constructor(
    private changeDataService: ChangeDataService,
    private router: Router,
    public clientInfo: ClientInfoService,
    public alertController: AlertController,
    ) {
      let content = this.clientInfo.clientInfo[0].content;
      this.fname = content[0][1];
      this.lname = content[1][1];
      this.birth = content[2][1];
      this.address = content[3][1];
      this.phoneNumber = content[4][1];
      this.email = content[5][1];
      this.passportNumber = content[6][1];
      this.passportType = content[7][1];
      this.expiryDate = content[8][1];
      // console.log('content of passport table is:',content);
  }

  // for QR code
  async createCredential(){
    if (this.allowEntry &&  this.allowExit && this.purpose && this.tempResidential && this.aggree && this.emergencyContact){
      this.continue = true;
      let content = new Map<string,string>([
        ['First Name', this.fname],
        ['Last Name', this.lname],
        ['Date of Birth', this.birth],
        ['Address', this.address],
        ['Phone number', this.phoneNumber],
        ['Email', this.email],
        ['Passport ID', this.passportNumber],
        ['Type of passport', this.passportType],
        ['Expiry date', this.expiryDate],
        ['Grant Evisa valid from (DD/MM/YYYY)', this.validFrom],
        ['To (DD/MM/YYYY)', this.validTo],
        ['Country to travel', this.country],
        ['Entry checkpoint', this.allowEntry],
        ['Exit checkpoint', this.allowExit],
        ['Purpose of entry', this.purpose],
        ['Intended temporary residential address in Malaysia', this.tempResidential],
        ['Emergency contact', this.emergencyContact]
      ])
      // console.log("content in submit-information is:",content);
      const credential = Credentials.create(
        'visa',
        'Asia Visa',
        'Express Visa Agency',
        '/assets/icon-credentials-e-visa-country-a.svg',
        [],
        content,
        "Passport and Visa",
        Credentials.authCreate(content,this.changeDataService.privateKeydApp)
      );
      const msg = CredentialRequestMessage.create(credential);
      //console.log(msg);
  
      this.flag = false;
      this.img = await msg.generateQR();
      this.linkCredential = await msg.universalLink();
    }
    else {
      this.alertWarning();
    }
  }

  async alertWarning() {
    const alert = await this.alertController.create({
      header: 'Warning!',
      message: 'Fill these informations to continue',
      buttons: [
        {
          text: 'Ok' ,
          cssClass: 'secondary',
          handler: () => {
          }
        }
      ],
      cssClass:"boxAlert"
    });

    await alert.present();
  }
  welcome(){
    this.clientInfo.haveCredential = true;
    this.router.navigate(['/payment']);
  }

  ionViewWillEnter(){
    //console.log("enter");
    this.flag = true;
  }

  toggleBoxCalendar(index:number){
    if (index == 1){
      // console.log('toggle 1');
      this.box1 = !this.box1;
    }
    else {
      // console.log('toggle 2');
      this.box2 = !this.box2;
    }
  }

  setDate(index:number){
    if (index == 1 && this.entryDate){
      // console.log('set 1');
      this.clientInfo.entryDate = this.entryDate;
      this.toggleBoxCalendar(1);
    }
    else if (index == 1 && !this.entryDate){
      // console.log('unset 1');
      this.toggleBoxCalendar(1);
    }

    if (index == 2 && this.exitDate){
      // console.log('set 2');
      this.clientInfo.exitDate = this.exitDate;
      this.toggleBoxCalendar(2);
    }
    else if (index == 2 && !this.exitDate){
      // console.log('unset 2');
      this.toggleBoxCalendar(2);
    } 
  }

  checkResult(param:string){
    if (param == "hard"){
      this.clientInfo.lightVerification = false;
    }
    else if (param == "light"){
      this.clientInfo.lightVerification = true;
    }
    this.router.navigate(['/check-result']);
  }

  applyForm(){
    this.router.navigate(['/home']);
  }

  deeplink(param:string){
    if (param == "login"){
      window.location = this.linkLogin
    }
    else {
      window.location = this.linkCredential
    }
  }
}
