import { Injectable } from '@angular/core';
import { Address, NetworkType } from 'tsjs-xpx-chain-sdk';

@Injectable({
  providedIn: 'root'
})
export class ChangeDataService {

  pubickeySiriusid =''; 
  addressSiriusid = '';
  name = ''; // Name of user when him sign up
  
  /**
   * For TEST_NET
   */
  publicKeydApp = 'FF9151BB0D3F6386BE3A9F35B3C7A5247A397804BC059D1F05B2FED06F51805A'; // private key: B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433
  privateKeydApp = 'B607FBD8536A742A4BD02B9029E7B59CA612B727E29E81805DBFE441929AD433';
  addressdApp: Address;
  apiNode = "";
  ws = 'wss://' + this.apiNode; //websocket
  //ws = "wss://bctestnet3.xpxsirius.io";
  
  /**
   * For local testing
   */
  // ws = 'ws://192.168.1.23:3000'; //local
  // publicKeydApp = '0750E2716F2CBADD19F9F5314944ABFDF005A5D3D6DAAB4D08E97F188AAA9300'; // privateKey: 4EBACFE2B723FDA8B4DFC7E7B8AA26AF9C8638DCC353463AFF89F03230AFD38E
  updateWebsocket(){
    this.ws = 'wss://' + this.apiNode;
  }
  constructor() { 
    this.addressdApp = Address.createFromPublicKey(this.publicKeydApp, NetworkType.TEST_NET);
  }
}
