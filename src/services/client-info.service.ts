import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ClientInfoService {

  name;
  numberForm = "CH2534";
  nation;
  purpose;
  entryDate;
  exitDate;

  clientInfo : any;
  
  haveCredential = false;
  lightVerification = false;
  
  constructor() { }
}
